__author__ = 'Agnei Silva'

import os

basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
WTF_CSRF_ENABLED = True
SECRET_KEY = 'k\xc0\xef\x8a;\xa9\xed^\x8a\x87\xaai\x888\n\xb0i\x1c\xa3\x10\\[\x9e\x8c\xe7\xd5\xf86=\xa3p0\x8a\x07\xc5}'

CURRENT_DIRECTORY = basedir
AG_HOST = os.environ.get('AGRAPH_HOST', 'localhost')
AG_PORT = int(os.environ.get('AGRAPH_PORT', '10035'))
AG_CATALOG = os.environ.get('AGRAPH_CATALOG', 'system')
AG_REPOSITORY = 'system'
AG_USER = 'admin' # super
AG_PASSWORD = 'pass' # 123
AG_REST_ENDPOINT = 'http://'+AG_HOST+':'+str(AG_PORT)+'/catalogs/'+AG_CATALOG+'/repositories/'+AG_REPOSITORY+\
                   '/statements'
AG_SPARQL_ENDPOINT = 'http://'+AG_HOST+':'+str(AG_PORT)+'/catalogs/'+AG_CATALOG+'/repositories/'+AG_REPOSITORY
RAISE_EXCEPTION_ON_VERIFY_FAILURE = False

DOWNLOAD_PATH = os.getenv('DOWNLOAD_PATH', 'C:/DOWNLOAD_PATH/') 

UPLOAD_FOLDER = '/files/'