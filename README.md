BioProv Prototype
Protótipo do BioProv


Provenance Architecture of Biodiversity Data, based on PROV-DM.
Arquitetura para proveniência de dados de biodiversidade, baseado no PROV-DM.

https://github.com/franzinc/docker-agraph

docker run -it -d --rm --shm-size 1g -v agdata:/agraph/data -v agconfig:/agraph/etc -e AGRAPH_SUPER_USER=admin -e AGRAPH_SUPER_PASSWORD=pass -p 10000-10035:10000-10035 --name agraph-instance-1 franzinc/agraph:v7.0.0

python3 -m pip install -r requirements.txt
python3 -m pip install pathlib 

Python3.9

https://sqliteonline.com/

 Pré requisitos
- Ao subir um servidor do Allegrograph configurar um usuário anonymous com todos as permissões

- Instalar o https://graphviz.org/download/ na maquina pois é dependencia para geração dos arquivos






DOCKER COMMANDS: 
docker build -t bioprov .
docker run -p 5000:5000 bioprov


--DEV
docker-compose -f docker-compose.yml --no-ansi build
 
docker-compose -f docker-compose.yml --no-ansi up -d --no-build --force-recreate --remove-orphans
 
docker-compose -f docker-compose.yml --no-ansi up -d --build --force-recreate --remove-orphans
 
docker-compose -f docker-compose.yml down

docker-compose -f docker-compose.yml push



--DEV
docker-compose -f docker-compose.nobuild.yml pull
docker-compose -f docker-compose.nobuild.yml --no-ansi up -d --no-build --force-recreate --remove-orphans
  
docker-compose -f docker-compose.nobuild.yml down
