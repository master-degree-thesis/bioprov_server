from app import db, app
from app.resources import parser_model
from passlib.apps import custom_app_context as pwd_context
import validators
import requests
import json
import uuid
from urllib.parse import urlparse
from urllib import parse
from itsdangerous import (TimedJSONWebSignatureSerializer as JsonSerializer, BadSignature, SignatureExpired)
from rdflib import URIRef, Literal, ConjunctiveGraph, BNode
from rdflib.namespace import RDF, FOAF
from rdflib.plugin import register, Serializer, Parser
import rdflib
register('json-ld', Serializer, 'rdflib_jsonld.serializer', 'JsonLDSerializer')
register('json-ld', Parser, 'rdflib_jsonld.parser', 'JsonLDParser')
from prov.model import ProvDocument
from prov.dot import prov_to_dot
from  app.resources.parser_model import prov_predicates, prov_type
import sys


# function to include identifier of a graph through URL.
def include_id_tuple(data,id):
    dict_graph = json.loads(data)
    cont_graph = 0
    for key,value in dict_graph.items():
        if key=="@graph":
            cont_graph += 1
            for graph in value:
                cont_id = 0
                for key2,value2 in graph.iteritems():
                    if key2=="@id":
                        cont_id += 1
                        break
                if cont_id==0:
                    graph["@id"] = id
    if cont_graph == 0:
        dict_graph["@id"] = id

    return json.dumps(dict_graph)


# User Class Definition
class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(64))
    email = db.Column(db.String(120), index=True, unique=True)
    token = db.Column(db.String(250))
    default_prefix = db.Column(db.String(250))

    def __init__(self, login, password, email, default_prefix, id=None):
        self.id = id
        self.login = login
        self.password_hash = self.hash_password(password)
        self.email = email
        self.default_prefix = default_prefix

    def __repr__(self):
        return '<User %r>' % self.login

    # Encrypt the password defined by the user
    @staticmethod
    def hash_password(password):
        return pwd_context.encrypt(password)

    # Verify the password sent by the user
    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    # Generate the authorization token to the user based in his ID
    def generate_auth_token(self, expiration=600):
        s = JsonSerializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    # Verify the authorization token sent by the user
    @staticmethod
    def verify_auth_token(token):
        s = JsonSerializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None    # valid token, but expired
        except BadSignature:
            return None    # invalid token
        user = User.query.get(data['id'])
        return user


#Graph Definition
class Graph(object):
    def __init__(self, id):
        if id:
            self.id = URIRef(id)
        else:
            self.id = None
        self.triples = []


# Bundle Class Definition - Bundle Definition http://www.w3.org/TR/prov-dm/#component4
class Resource(object):

    def __init__(self, id=None, context=None, graph=None):

        if id:
            if isinstance(id,URIRef):
                self.id = id
            elif validators.url(id):
                self.id = URIRef(id)
            elif id.startswith('_:'):
                self.id = BNode(id.replace('_:', ''))
            else:
                self.id =BNode(id)
        else:
            self.id = BNode()

        if graph:
            self.graph = graph
        else:
            self.graph = ConjunctiveGraph()
        self.type = None

        if context:
            if context == 'null':
                self.context = 'null'
            elif isinstance(context,URIRef):
                self.context = context
            else:
                self.context = URIRef(context).n3()
            #self.context = context if isinstance(context,URIRef) else URIRef(context).n3()
        else:
            self.context = None

    def __repr__(self):
        return '<Resource %r>' % self.id

    def save_properties(self,data):

        self.graph.parse(data=data,format='json-ld')
        if self.update():
            return self.query_by_id()
        else:
            return False

    def update(self):
        if self.validate_fields():
            params={'context': self.context }
            self.graph.add((self.id, RDF.type, self.type))
            body = self.graph.serialize(format='turtle')
            headers = {'Content-Type':'text/turtle'}
            response = requests.post(app.config['AG_REST_ENDPOINT'], headers=headers, params=params, data=body)
            if response:
                return True
            else:
                return False

    def delete_by_id(self, predicate=None, object=None):

        params = {'subj':self.id.n3(), 'context': self.context }
        if predicate:
            try:
                params['pred'] = predicate.n3() if isinstance(predicate,URIRef) else URIRef(predicate).n3()
            except:
                params['pred'] = predicate
        if object:
            try:
                params['obj'] = object.n3() if isinstance(object, URIRef) else URIRef(object).n3()
            except:
                params['obj'] = object
        headers = {'Accept':'application/json'}
        response = requests.delete(app.config['AG_REST_ENDPOINT'],headers=headers, params=params)
        return '{"deleted_triples": ' + response.text + ' }'

    def delete(self):

        params = {'context': self.context}
        headers = {'Accept':'application/json'}
        response = requests.delete(app.config['AG_REST_ENDPOINT'], headers=headers, params=params)
        return '{"deleted_triples": ' + response.text + ' }'

    def query_by_id(self):

        params = {}

        if self.context:
            params['context'] = self.context

        if self.id:
            params['subj'] = self.id.n3()

        headers = {'Content-Type':'text/turtle'}
        response = requests.get(app.config['AG_REST_ENDPOINT'], headers=headers, params=params)

        if len(response.text) > 0:
            graph = ConjunctiveGraph()
            graph.parse(data=response.text, format='turtle')
            # return json.loads(graph.serialize(format="json-ld", auto_compact=True, indent=4))
            graph.bind('dcterms', 'http://purl.org/dc/terms/')
            graph.bind('prov', 'http://www.w3.org/ns/prov#')
            graph.bind('bioprov', 'http://bioprov.org/')
            #graph.bind('bioprov_poli', 'http://bioprov.poli.usp.br')
            graph.bind('poli', 'http://poli.usp.br/')
            #graph.bind('ib_usp', 'http://ib.usp.br/')

            return graph.serialize(format="json-ld", auto_compact=True, indent=4).decode()
        else:
            return '{}'

    def query(self):

        params = {}

        if self.context:
            params['context'] = self.context

        params['obj'] = self.type.n3()

        headers = {'Content-Type':'text/turtle'}
        response = requests.get(app.config['AG_REST_ENDPOINT'], headers=headers, params=params)

        if len(response.text) > 0:
            graph = ConjunctiveGraph()
            graph.parse(data=response.text,format='turtle')
            # return json.loads(graph.serialize(format="json-ld", auto_compact=True, indent=4))
            graph.bind('dcterms','http://purl.org/dc/terms/')
            graph.bind('prov','http://www.w3.org/ns/prov#')
            return graph.serialize(format="json-ld", auto_compact=True, indent=4).decode()
        else:
            return '{}'

    def validate_fields(self):
        if self.id:
            return True
        else:
            return False

    def add_triple(self, subject, predicate, object):

        # Subject Validation - Possible Values = URIRef, BNode, URL (str)
        if isinstance(subject,URIRef) or isinstance(subject,BNode):
            subj = subject
        elif regex.search(subject):
            subj = URIRef(subject)
        else:
            subj = None
            raise ValueError('Subject should be a URIRef, BNode or URL.')

        # Predicate Validation - Possible Values = URIRef, URL (str)
        if isinstance(predicate,URIRef):
            pred = predicate
        elif regex.search(predicate):
            pred = URIRef(predicate)
        else:
            pred = None
            raise ValueError('Predicate should be a URIRef, BNode or URL.')

        # Object Validation - Possible Values = URIRef, Literal, str
        if isinstance(object,URIRef) or isinstance(object,Literal):
            obj = object
        elif regex.search(object):
            obj = URIRef(object)
        else:
            obj = Literal(object)

        # Add triple in the Graph.
        self.graph.add((subj, pred, obj))

    def add_quad(self,subject, predicate, object):

        # Subject Validation - Possible Values = URIRef, BNode, URL (str)
        if isinstance(subject,URIRef) or isinstance(subject,BNode):
            subj = subject
        elif regex.search(subject):
            subj = URIRef(subject)
        else:
            subj = None
            raise ValueError('Subject should be a URIRef, BNode or URL.')

        # Predicate Validation - Possible Values = URIRef, URL (str)
        if isinstance(predicate,URIRef):
            pred = predicate
        elif regex.search(predicate):
            pred = URIRef(predicate)
        else:
            pred = None
            raise ValueError('Predicate should be a URIRef, BNode or URL.')

        # Object Validation - Possible Values = URIRef, Literal, str
        if isinstance(object,URIRef) or isinstance(object,Literal):
            obj = object
        elif regex.search(object):
            obj = URIRef(object)
        else:
            obj = Literal(object)

        # Add quad in the Graph.
        self.graph.add((subj, pred, obj, self.id.n3()))

    def add_properties(self,predicate,object):

        # # Subject Validation - Possible Values = URIRef, BNode, URL (str)
        # if isinstance(self.id,URIRef) or isinstance(self.id,BNode):
        #     subj = self.id
        # elif regex.search(self.id):
        #     subj = URIRef(self.id)
        # else:
        #     subj = None
        #     raise ValueError('Bundle ID should be a URIRef, BNode or URL.')

        # Predicate Validation - Possible Values = URIRef, URL (str)
        if isinstance(predicate,URIRef):
            pred = predicate
        elif regex.search(predicate):
            pred = URIRef(predicate)
        else:
            pred = None
            raise ValueError('Predicate should be a URIRef, BNode or URL.')

        # Object Validation - Possible Values = URIRef, Literal, str
        if isinstance(object,URIRef) or isinstance(object,Literal):
            obj = object
        elif regex.search(object):
            obj = URIRef(object)
        else:
            obj = Literal(object)

        # Add triple in the Graph.
        self.graph.add((self.id.n3(), predicate, object))

    def delete_triple(self, subject, predicate, object):

        # Subject Validation - Possible Values = URIRef, BNode, URL (str)
        if isinstance(subject,URIRef) or isinstance(subject,BNode):
            subj = subject
        elif regex.search(subject):
            subj = URIRef(subject)
        else:
            subj = None
            raise ValueError('Subject should be a URIRef, BNode or URL.')

        # Predicate Validation - Possible Values = URIRef, URL (str)
        if isinstance(predicate,URIRef):
            pred = predicate
        elif regex.search(predicate):
            pred = URIRef(predicate)
        else:
            pred = None
            raise ValueError('Predicate should be a URIRef, BNode or URL.')

        # Object Validation - Possible Values = URIRef, Literal, str
        if isinstance(object,URIRef) or isinstance(object,Literal):
            obj = object
        elif regex.search(object):
            obj = URIRef(object)
        else:
            obj = Literal(object)

        # Add triple in the Graph.
        self.graph.remove((subj, pred, obj))

    def delete_quad(self,subject, predicate, object):

        # Subject Validation - Possible Values = URIRef, BNode, URL (str)
        if isinstance(subject,URIRef) or isinstance(subject,BNode):
            subj = subject
        elif regex.search(subject):
            subj = URIRef(subject)
        else:
            subj = None
            raise ValueError('Subject should be a URIRef, BNode or URL.')

        # Predicate Validation - Possible Values = URIRef, URL (str)
        if isinstance(predicate,URIRef):
            pred = predicate
        elif regex.search(predicate):
            pred = URIRef(predicate)
        else:
            pred = None
            raise ValueError('Predicate should be a URIRef, BNode or URL.')

        # Object Validation - Possible Values = URIRef, Literal, str
        if isinstance(object,URIRef) or isinstance(object,Literal):
            obj = object
        elif regex.search(object):
            obj = URIRef(object)
        else:
            obj = Literal(object)

        # Add quad in the Graph.
        self.graph.remove((subj, pred, obj, self.id.n3()))

    def delete_properties(self, predicate, object):
        if isinstance(predicate,URIRef):
            pred = predicate
        elif regex.search(predicate):
            pred = URIRef(predicate)
        else:
            pred = None
            raise ValueError('Predicate should be a URIRef, BNode or URL.')

        # Object Validation - Possible Values = URIRef, Literal, str
        if isinstance(object,URIRef) or isinstance(object,Literal):
            obj = object
        elif regex.search(object):
            obj = URIRef(object)
        else:
            obj = Literal(object)

        # Add triple in the Graph.
        self.graph.remove((self.id.n3(), predicate, object))

    def query_graph(self):
        if self.id:
            payload = {'context':self.id.n3()}
            response = requests.get(app.config['AG_REST_ENDPOINT'],params=payload)
            if response:
                graph = ConjunctiveGraph()
                graph.parse(data=response,format='turtle')
                return json.loads(graph.serialize(format="json-ld", auto_compact=True, indent=4).Decode('UTF-8'))
            else:
                return {}


class Bundle(Resource):
    def __init__(self, id=None, context='null', graph=None):
        super(Bundle, self).__init__(id=id, context=context, graph=graph)
        self.type = URIRef('http://www.w3.org/ns/prov#Bundle')
        # self.context = 'null'

        def __repr__(self):
            return '<Bundle %r>' % self.id

    @staticmethod
    def generate_id():
        return 'http://bioprov.org/' + str(uuid.uuid4())
    def query_data_by_id(self):

        params = dict()

        # if self.context:
        #     params['context'] = self.context

        # if self.id:
        #     params['subj'] = self.id.n3()

        print(self.context, file=sys.stderr)
        print(self.id, file=sys.stderr)

        params['query'] = ''' SELECT ?x ?z WHERE { %s ?x ?z } ''' % self.context

        headers = {'Content-Type':'text/turtle'}
        response = requests.get(app.config['AG_REST_ENDPOINT'], headers=headers, params=params)

        graph = ConjunctiveGraph()
        if len(response.text) > 0:
            graph.parse(data=response.text, format='turtle')
            # return json.loads(graph.serialize(format="json-ld", auto_compact=True, indent=4))
            graph.bind('dcterms', 'http://purl.org/dc/terms/')
            graph.bind('prov', 'http://www.w3.org/ns/prov#')
            graph.bind('bioprov', 'http://bioprov.org/')
            #graph.bind('bioprov_poli', 'http://bioprov.poli.usp.br')
            graph.bind('poli', 'http://poli.usp.br/')
            #graph.bind('ib_usp', 'http://ib.usp.br/')

        return graph


class Entity(Resource):
    def __init__(self, id=None, context=None, graph=None):
        super(Entity, self).__init__(id=id, context=context, graph=graph)
        self.type = URIRef('http://www.w3.org/ns/prov#Entity')

    def __repr__(self):
        return '<Entity %r>' % self.id

    @staticmethod
    def generate_id():
        return 'http://bioprov.org/entities/' + str(uuid.uuid4())


class Activity(Resource):
    def __init__(self, id=None, context=None, graph=None):
        super(Activity, self).__init__(id=id, context=context, graph=graph)
        self.type = URIRef('http://www.w3.org/ns/prov#Activity')

    def __repr__(self):
        return '<Activity %r>' % self.id

    @staticmethod
    def generate_id():
        return 'http://bioprov.org/activities/' + str(uuid.uuid4())


class Agent(Resource):
    def __init__(self, id=None, context=None, graph=None):
        super(Agent, self).__init__(id=id, context=context, graph=graph)
        self.type = URIRef('http://www.w3.org/ns/prov#Agent')

    def __repr__(self):
        return '<Agent %r>' % self.id

    @staticmethod
    def generate_id():
        return 'http://bioprov.org/agents/' + str(uuid.uuid4())


class Sparql(object):

    @staticmethod
    def query(query, headers):
    # Corrigir problema de serializacao. O metodo aceita apenas o sparql results e nao o rdf xml.
        params = dict()
        params['query'] = query

        #headers = {'Accept': 'application/json'}
        response = requests.get(app.config['AG_SPARQL_ENDPOINT'], headers=headers, params=params)

        # if len(response.text) > 0:
        #     graph = ConjunctiveGraph()
        #     graph.parse(data=response.text, format='json')
        #     # return json.loads(graph.serialize(format="json-ld", auto_compact=True, indent=4))
        #     graph.bind('dcterms', 'http://purl.org/dc/terms/')
        #     graph.bind('prov', 'http://www.w3.org/ns/prov#')
        #     graph.bind('bioprov', 'http://bioprov.org/')
        #     return graph.serialize(format="json-ld", auto_compact=True, indent=4).decode()
        # else:
        #     return '{}'

        return response.text


class Provenance(Resource):

    def __init__(self, id=None, context=None, graph=None):
        super(Provenance, self).__init__(id=id, context=context, graph=graph)

    def bidirecional_provenance(self):
        params = dict()
        params['query'] = '''   PREFIX prov: <http://www.w3.org/ns/prov#>
                                describe ?s1 ?p1 ?o1 where {
                                {?s1 ?p1 ?o1 .}
                                optional {?s0 ?p ?s1 .}
                                filter (?s1 = <%s>)
                                }''' % self.id

        headers = {'Accept': 'application/rdf+xml'}
        response = requests.get(app.config['AG_SPARQL_ENDPOINT'], headers=headers, params=params)

        if len(response.text) > 0:
            graph = ConjunctiveGraph()
            graph.parse(data=response.text, format='xml')
            # return json.loads(graph.serialize(format="json-ld", auto_compact=True, indent=4))
            graph.bind('dcterms', 'http://purl.org/dc/terms/')
            graph.bind('prov', 'http://www.w3.org/ns/prov#')
            graph.bind('bioprov', 'http://bioprov.org/')
            #graph.bind('bioprov_poli', 'http://bioprov.poli.usp.br')
            graph.bind('poli', 'http://poli.usp.br/')
            #graph.bind('ib_usp', 'http://ib.usp.br/')
            #return graph.serialize(format="json-ld", auto_compact=True, indent=4).decode()
            return graph
        else:
            return '{}'

    def outgoing_provenance(self):
        params = dict()
        params['query'] = '''  describe ?s1 ?p1 ?o1
                               where {
                                        {?s1 ?p1 ?o1 .}
                                        filter (?s1 = <%s>)
                                }''' % self.id

        headers = {'Accept': 'application/rdf+xml'}
        response = requests.get(app.config['AG_SPARQL_ENDPOINT'], headers=headers, params=params)

        if len(response.text) > 0:
            graph = ConjunctiveGraph()
            graph.parse(data=response.text, format='xml')
            # return json.loads(graph.serialize(format="json-ld", auto_compact=True, indent=4))
            graph.bind('dcterms', 'http://purl.org/dc/terms/')
            graph.bind('prov', 'http://www.w3.org/ns/prov#')
            graph.bind('bioprov', 'http://bioprov.org/')
            #graph.bind('bioprov_poli', 'http://bioprov.poli.usp.br')
            graph.bind('poli', 'http://poli.usp.br/')
            #graph.bind('ib_usp', 'http://ib.usp.br/')
            #return graph.serialize(format="json-ld", auto_compact=True, indent=4).decode()
            return graph
        else:
            return '{}'

    def incoming_provenance(self):
        params = dict()
        params['query'] = '''  describe ?s1 ?p1 ?o1 where {
                                {?s1 ?p1 ?o1 .}
                                filter (?o1 = <%s>)
                                }''' % self.id

        headers = {'Accept': 'application/rdf+xml'}
        response = requests.get(app.config['AG_SPARQL_ENDPOINT'], headers=headers, params=params)

        if len(response.text) > 0:
            graph = ConjunctiveGraph()
            graph.parse(data=response.text, format='xml')
            # return json.loads(graph.serialize(format="json-ld", auto_compact=True, indent=4))
            graph.bind('dcterms', 'http://purl.org/dc/terms/')
            graph.bind('prov', 'http://www.w3.org/ns/prov#')
            graph.bind('bioprov', 'http://bioprov.org/')
            #graph.bind('bioprov_poli', 'http://bioprov.poli.usp.br')
            graph.bind('poli', 'http://poli.usp.br/')
            #graph.bind('ib_usp', 'http://ib.usp.br/')
            #return graph.serialize(format="json-ld", auto_compact=True, indent=4).decode()
            return graph
        else:
            return '{}'

    def resource_details(self):
        params = dict()
        params['query'] = '''  select ?s1 ?p1 ?o1 where {
                                {?s1 ?p1 ?o1 .}
                                filter (?s1 = <%s>)
                                }''' % self.id

        headers = {'Accept': 'application/rdf+xml'}
        response = requests.get(app.config['AG_SPARQL_ENDPOINT'], headers=headers, params=params)

        if len(response.text) > 0:
            graph = ConjunctiveGraph()
            graph.parse(data=response.text, format='xml')
            # return json.loads(graph.serialize(format="json-ld", auto_compact=True, indent=4))
            graph.bind('dcterms', 'http://purl.org/dc/terms/')
            graph.bind('prov', 'http://www.w3.org/ns/prov#')
            graph.bind('bioprov', 'http://bioprov.org/')
            #graph.bind('bioprov_poli', 'http://bioprov.poli.usp.br')
            graph.bind('poli', 'http://poli.usp.br/')
            #graph.bind('ib_usp', 'http://ib.usp.br/')
            #return graph.serialize(format="json-ld", auto_compact=True, indent=4).decode()
            return graph
        else:
            return '{}'

    @staticmethod
    def parser_prov_output(graph=ConjunctiveGraph()):

        # create Prov document
        document = ProvDocument()

        namespaces = []
        for item in graph.namespaces():
            namespaces.append(str(item[1]))

        exist = True


        try:
          for s, p, o in graph:
            #if str(s)[: str(s).rfind('/') + 1] not in namespaces:
            #    graph.bind(parser_model.id_generator(4), str(s)[: str(s).rfind('/') + 1])
            #    namespaces.append(str(s)[: str(s).rfind('/') + 1])
            for item in namespaces:
                if str(item) in str(s):
                    exist = True
                    break
                else:
                    exist = False
            if not exist:
                url = urlparse(str(s))
                if url.fragment:
                    url_namespace = url.geturl().rsplit(url.fragment)[0]
                    graph.bind(url.netloc, url_namespace)
                    namespaces.append(url_namespace)
                elif url.netloc:
                    graph.bind(url.netloc, url.geturl())
                    namespaces.append(url.geturl())
                elif url.scheme:
                    graph.bind(url.scheme, url.scheme)
                    namespaces.append(url.scheme)
                else:
                    graph.bind(parser_model.id_generator(4), str(s)[: str(s).rfind('/') + 1])
                    namespaces.append(str(s)[: str(s).rfind('/') + 1])

                #graph.bind(str(s)[: str(s).rfind('/') + 1], str(s)[: str(s).rfind('/') + 1])
                #namespaces.append(str(s)[: str(s).rfind('/') + 1])
        except NameError:
          print("Variable x is not defined", sys.exc_info()[0])
        
        

        # include namespaces
        for namespace in graph.namespaces():
            if namespace[0] != "prov" and namespace[0] != "xsd":
                document.add_namespace(namespace[0], namespace[1])

        # identify resources and their types and include in a dictionary
        resources = dict()
        #result = graph.query('''
        #select ?s ?p ?o
        #where {{?s ?p ?o}
        #       filter(?p=<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>)
        #       }''')
        #result = graph.query('select ?s ?o where {?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?o}')

        for s, p, o in graph:
            if str(p) == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type':
                resources[str(s)] = {}
                resources[str(s)]['type'] = str(o)
                resources[str(s)]['properties'] = {}

        # identify properties of resources and include in a dictionary
        result = graph.query('''
        select ?s ?p ?o
        where {{?s ?p ?o}
                FILTER (!STRSTARTS(STR(?p), "http://www.w3.org/ns/prov#"))
                FILTER (!STRSTARTS(STR(?p), "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"))
                }''')

        for s, p, o in result:
            #if 'properties' not in resources[str(s)]:
            #    resources[str(s)]['properties'] = {}
            resources[str(s)]['properties'][str(p)] = str(o) #if isinstance(o, Literal) else '<' + str(o) + '>'

        # Include resources and their properties in the PROV Document
        for k, value in resources.items():
            if value['type'] in prov_type:
                getattr(document, prov_type[value['type']])(identifier=k, other_attributes=value['properties'])

        # Include PROV statements in the PROV Document
        result = graph.query('''
        select ?s ?p ?o
        where {{?s ?p ?o}
                FILTER (STRSTARTS(STR(?p), "http://www.w3.org/ns/prov#"))
                }
        order by ?p''')

        for s, p, o in result:
            if str(p) in prov_predicates:
                getattr(document, prov_predicates[str(p)])(str(s), str(o))

        #dot = prov_to_dot(document)
        #dot.write_png('document.png')

        return document


# Document Class Definition - Used to manage the import/export of RDF graphs
class Document(object):

    def __init__(self):
        self.id = None
        self.context = None
        self.content = None

    def __repr__(self):
        return '<Document %r>' % self.id
