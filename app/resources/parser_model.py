import string
import random

prov_predicates = dict()
prov_predicates['http://www.w3.org/ns/prov#wasDerivedFrom'] = 'wasDerivedFrom'
prov_predicates['http://www.w3.org/ns/prov#wasGeneratedBy'] = 'wasGeneratedBy'
prov_predicates['http://www.w3.org/ns/prov#wasAttributedTo'] = 'wasAttributedTo'
prov_predicates['http://www.w3.org/ns/prov#used'] = 'used'
prov_predicates['http://www.w3.org/ns/prov#endedAtTime'] = 'wasEndedBy'
prov_predicates['http://www.w3.org/ns/prov#startedAtTime'] = 'wasStartedBy'
prov_predicates['http://www.w3.org/ns/prov#wasInformedBy'] = 'wasInformedBy'
prov_predicates['http://www.w3.org/ns/prov#wasAssociatedWith'] = 'wasAssociatedWith'
prov_predicates['http://www.w3.org/ns/prov#actedOnBehalfOf'] = 'actedOnBehalfOf'


prov_type = dict()
prov_type['http://www.w3.org/ns/prov#Entity'] = 'entity'
prov_type['http://www.w3.org/ns/prov#Activity'] = 'activity'
prov_type['http://www.w3.org/ns/prov#Agent'] = 'agent'


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))
