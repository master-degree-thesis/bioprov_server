from flask.helpers import make_response
from flask import send_file
__author__ = 'Agnei Silva'

from app.resources.models import *
from flask import jsonify, request
from flask_restful import abort, fields, Resource, reqparse, marshal
from rdflib.plugin import register, Serializer, Parser
register('json-ld', Serializer, 'rdflib_jsonld.serializer', 'JsonLDSerializer')
register('json-ld', Parser, 'rdflib_jsonld.parser', 'JsonLDParser')
from app.views import g
from app import auth
import os
from pathlib import Path
import sys


# Structure definition of User objects serialization
user_fields = {
    'login': fields.String,
    'email': fields.String,
    'default_prefix': fields.String,
    'uri': fields.Url('user', absolute=True)
}

# Structure definition of Bundle objects serialization
bundle_fields = {
    'dcterms:identifier': fields.String,
    'dcterms:title': fields.String,
    'dcterms:subject': fields.String,
    'dcterms:description': fields.String,
    'dcterms:creator': fields.String,
    'dcterms:created': fields.String,
    'dcterms:modified': fields.String,
    'dcterms:contributor': fields.String,
    'resources': fields.Url('bundle', absolute=True)
}

prefix_namespaces = {'prov':'http://www.w3.org/ns/prov#', 'dcterms': 'http://purl.org/dc/terms/'}


def replace_prefix(data):

    if data and not '//' in data:
        pos = data.find(':')
        prefix = data[:pos]
        data = data.replace(':','')
        return data.replace(prefix,prefix_namespaces[prefix])
    return data


# Resource Definitions

# Collection of Users
class UsersAPI(Resource):

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('login', type=str, required=True, help='No login provided', location='json')
        self.reqparse.add_argument('password', type=str, required=True, help='No password provided', location='json')
        self.reqparse.add_argument('email', type=str, required=True, help='No email provided', location='json')
        self.reqparse.add_argument('default_prefix', type=str, required=True, help='No default_prefix provided',
                                   location='json')
        super(UsersAPI, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        user = {
            'id': db.session.query(User).count() + 1,
            'login': args['login'],
            'email': args['email'],
            'default_prefix': args['default_prefix']
        }
        newuser = User(login=user['login'], password=args['password'], email=user['email'],
                       default_prefix=user['default_prefix'])
        db.session.add(newuser)
        db.session.commit()
        return {'user': marshal(user, user_fields)}, 201


# User
class UserAPI(Resource):

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('login', type=str, required=True, help='No login provided', location='json')
        self.reqparse.add_argument('password', type=str, required=True, help='No password provided', location='json')
        self.reqparse.add_argument('email', type=str, required=True, help='No email provided', location='json')
        self.reqparse.add_argument('default_prefix', type=str, required=True, help='No default_prefix provided',
                                   location='json')
        super(UserAPI, self).__init__()

    def get(self, id):
        user = User.query.get(id)
        if not user:
            abort(404)
        else:
            return {'user': marshal(user, user_fields)}

    def delete(self, id):
        user = User.query.filter_by(id=id).first()
        if not user:
            abort(404)
        else:
            db.session.delete(user)
            db.session.commit()
            return jsonify({'result': True})

    def put(self, id):
        args = self.reqparse.parse_args()
        user = User.query.filter_by(id=id).first()
        if not user:
            user = {
            'id': id,
            'login': args['login'],
            'email': args['email'],
            'default_prefix': args['default_prefix']
            }
            newuser = User(login=user['login'], password=args['password'], email=user['email'],
                       default_prefix=user['default_prefix'], id=id)
            db.session.add(newuser)
            db.session.commit()
            return {'user': marshal(user, user_fields)}, 201
        else:
            args = self.reqparse.parse_args()
            user.login = args['login'] if args['login'] else user.login
            user.password_hash = user.hash_password(args['password']) if args['password'] else user.password_hash
            user.email = args['email'] if args['email'] else user.email
            user.default_prefix = args['default_prefix'] if args['default_prefix'] else user.default_prefix
            db.session.commit()
            return {'user': marshal(user, user_fields)}, 200


# Authentication Token
class TokenAPI(Resource):
    decorators = [auth.login_required]

    def get(self):
        token = g.user.generate_auth_token(600)
        g.user.token = token
        db.session.commit()
        return jsonify({'token': token.decode('ascii'), 'duration': 600})


# Collection of Resources
class ResourcesAPI(Resource):
     pass


# Resource
class ResourceAPI(Resource):

    def __init__(self):
        super(BundleAPI, self).__init__()

    def get(self, id):
        bundle = Bundle(id=id)
        result = bundle.query()
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)

    def delete(self, id):
        bundle = Bundle(id=id)
        data = request.data
        result = bundle.delete_bulk_properties(id)
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)

    def put(self, id):
        bundle = Bundle(id=id)
        data = request.data
        result = bundle.save_properties(data)
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)


# Collection of Bundles
class BundlesAPI(Resource):

    def __init__(self):
        super(BundlesAPI, self).__init__()


    def get(self):
        bundle = Bundle()
        result = bundle.query()
        return json.loads(result)

    def delete(self):
        bundle = Bundle()
        result = bundle.delete()
        return json.loads(result)

    def post(self):
        if '@id' in json.loads(request.data):
            id = json.loads(request.data)['@id']
        else:
            id = Bundle.generate_id()

        bundle = Bundle(id=id, context='null')
        data = include_id_tuple(request.data,id)
        result = bundle.save_properties(data)
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)


# Bundle
class BundleAPI(Resource):

    def __init__(self):
        super(BundleAPI, self).__init__()

    def get(self, id):
        bundle = Bundle(id=id, context='null')
        result = bundle.query_by_id()
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)

    def delete(self, id):
        bundle = Bundle(id=id)
        result = bundle.delete_by_id()
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)

    def put(self, id):
        bundle = Bundle(id=id, context='null')
        data = include_id_tuple(request.data,id)
        result = bundle.save_properties(data)
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)


# Bundle Data
class BundleDataAPI(Resource):

    def __init__(self):
        super(BundleDataAPI, self).__init__()

    def get(self, id):
        bundle = Bundle(context=id)
        bundle.id = None
        graph = bundle.query_data_by_id()
        graph.bind('dcterms', 'http://purl.org/dc/terms/')
        graph.bind('prov', 'http://www.w3.org/ns/prov#')
        graph.bind('bioprov', 'http://bioprov.org/')
        graph.bind('teste', 'http://teste.com/')
        graph.bind('foaf', 'http://xmlns.com/foaf/0.1/')
        #graph.bind('bioprov_poli', 'http://bioprov.poli.usp.br')
        #graph.bind('poli_usp', 'http://poli.usp.br/')
        #graph.bind('ib_usp', 'http://ib.usp.br/')

        if 'format' not in request.args or request.args['format'] == 'json':
            return json.loads(graph.serialize(format="json-ld", auto_compact=True, indent=4).decode())

        elif request.args['format'] == 'report':
            document = Provenance.parser_prov_output(graph)
            response = make_response(document.get_provn())
            response.headers['content-type'] = 'text/plain'
            return response

        elif request.args['format'] == 'svg':
            document = Provenance.parser_prov_output(graph)
            dot = prov_to_dot(document)
            file_name = app.config['DOWNLOAD_PATH'] + 'graph_{0}.svg'.format(str(id[id.rfind('/')+1:]))
            dot.write(file_name, format='svg')
            return send_file(file_name, mimetype='image/svg+xml')

        elif request.args['format'] == 'png':
            document = Provenance.parser_prov_output(graph)
            dot = prov_to_dot(document)
            file_name = app.config['DOWNLOAD_PATH'] + 'graph_{0}.png'.format(str(id[id.rfind('/')+1:]))
            dot.write_png(file_name)
            return send_file(file_name, mimetype='image/png')

        elif request.args['format'] == 'rdf':
            response = make_response(graph.serialize(format="turtle"))
            response.headers['content-type'] = 'text/plain'
            return response

        else:
            abort(404, message="Format {} invalid.".format(request.args['format']))


# Collection of Entities
class EntitiesAPI(Resource):

    def __init__(self):
        super(EntitiesAPI, self).__init__()

    def get(self):

        entity = Entity(context=request.args['bundle'] if request.args.get('bundle') else None)
        #if request.args.get('bundle'):
        #    entity = Entity(context=request.args['bundle'])
        result = entity.query()
        return json.loads(result)

    # def delete(self):
    #     if request.args['bundle']:
    #         entity = Entity(context=request.args['bundle'])
    #     result = entity.delete()
    #     return json.loads(result)
    def post(self):
        if '@id' in json.loads(request.data):
            id = json.loads(request.data)['@id']
        else:
            id = Entity.generate_id()

        entity = Entity(id=id, context=request.args['bundle'] if request.args.get('bundle') else None)
        data = include_id_tuple(request.data,id)
        result = entity.save_properties(data)
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)


# Entity
class EntityAPI(Resource):

    def __init__(self):
        super(EntityAPI, self).__init__()

    def get(self, id):

        entity = Entity(context=request.args['bundle'] if request.args.get('bundle') else None, id=id)
        #if request.args.get('bundle'):
        #    entity = Entity(context=request.args['bundle'], id=id)
        result = entity.query_by_id()
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)

    def delete(self, id):

        if request.args.get('bundle'):
            entity = Entity(context=request.args['bundle'], id=id)

        filter = []

        predicate = request.args.get('predicate') if request.args.get('predicate') else None
        predicate = replace_prefix(predicate)

        object = request.args.get('object') if request.args.get('object') else None
        result = entity.delete_by_id(predicate,object)
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)

    def put(self, id):
        if request.args['bundle']:
            entity = Entity(context=request.args['bundle'], id=id)
        data = include_id_tuple(request.data,id)
        result = entity.save_properties(data)
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)



    # Collection of Entities


# Collection of Entities
class FilesAPI(Resource):

    ALLOWED_FILE_EXT = ["pdf", "txt", "xls", "xlsx", "csv", "shp", "py", "doc", "r", "java", "js", "zip", "rar"]

    def __init__(self):
       pass

    def get(self):
        pass

    def post(self):
        try:
            uploaded_file = request.files['file']
            if uploaded_file and uploaded_file.mimetype.split("/")[1] in self.ALLOWED_FILE_EXT:
                uploaded_file.save(os.path.join(app.config['UPLOAD_FOLDER'], uploaded_file))

            else:
                abort(400, **{"message": "Invalid file format."})
        except Exception as e:
            abort(400, **{"message": "Invalid file format."})


# Collection of Activities
class ActivitiesAPI(Resource):
    def __init__(self):
        super(ActivitiesAPI, self).__init__()

    def get(self):
        activity = Activity(context=request.args['bundle'] if request.args.get('bundle') else None)
        #if request.args.get('bundle'):
        #    entity = Entity(context=request.args['bundle'])
        result = activity.query()
        return json.loads(result)

    # def delete(self):
    #     if request.args['bundle']:
    #         entity = Entity(context=request.args['bundle'])
    #     result = entity.delete()
    #     return json.loads(result)
    def post(self):
        if '@id' in json.loads(request.data):
            id = json.loads(request.data)['@id']
        else:
            id = Activity.generate_id()

        activity = Activity(id=id, context=request.args['bundle'] if request.args.get('bundle') else None)
        data = include_id_tuple(request.data,id)
        result = activity.save_properties(data)
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)


# Activity
class ActivityAPI(Resource):

    def __init__(self):
        super(ActivityAPI, self).__init__()

    def get(self, id):
        activity = Activity(context=request.args['bundle'] if request.args.get('bundle') else None, id=id)
        #if request.args.get('bundle'):
        #    entity = Entity(context=request.args['bundle'], id=id)
        result = activity.query_by_id()
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)

    def delete(self, id):

        if request.args.get('bundle'):
            activity = Activity(context=request.args['bundle'], id=id)

        filter = []

        predicate = request.args.get('predicate') if request.args.get('predicate') else None
        predicate = replace_prefix(predicate)

        object = request.args.get('object') if request.args.get('object') else None
        result = activity.delete_by_id(predicate,object)
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)

    def put(self, id):
        if request.args['bundle']:
            activity = Activity(context=request.args['bundle'], id=id)
        data = include_id_tuple(request.data,id)
        result = activity.save_properties(data)
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)


# Collection of Agents
class AgentsAPI(Resource):
    def __init__(self):
        super(AgentsAPI, self).__init__()

    def get(self):
        agent = Agent(context=request.args['bundle'] if request.args.get('bundle') else None)
        #if request.args.get('bundle'):
        #    entity = Entity(context=request.args['bundle'])
        result = agent.query()
        return json.loads(result)

    # def delete(self):
    #     if request.args['bundle']:
    #         entity = Entity(context=request.args['bundle'])
    #     result = entity.delete()
    #     return json.loads(result)
    def post(self):
        if '@id' in json.loads(request.data):
            id = json.loads(request.data)['@id']
        else:
            id = Agent.generate_id()

        agent = Agent(id=id, context=request.args['bundle'] if request.args.get('bundle') else None)
        data = include_id_tuple(request.data,id)

        #print(request.args['bundle'], file=sys.stderr)

        result = agent.save_properties(data)
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)


# Agent
class AgentAPI(Resource):

    def __init__(self):
        super(AgentAPI, self).__init__()

    def get(self, id):
        agent = Agent(context=request.args['bundle'] if request.args.get('bundle') else None, id=id)
        #if request.args.get('bundle'):
        #    entity = Entity(context=request.args['bundle'], id=id)
        result = agent.query_by_id()
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)

    def delete(self, id):

        if request.args.get('bundle'):
            agent = Agent(context=request.args['bundle'], id=id)

        filter = []

        predicate = request.args.get('predicate') if request.args.get('predicate') else None
        predicate = replace_prefix(predicate)

        object = request.args.get('object') if request.args.get('object') else None
        result = agent.delete_by_id(predicate,object)
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)

    def put(self, id):
        if request.args['bundle']:
            agent = Agent(context=request.args['bundle'], id=id)
        data = include_id_tuple(request.data,id)
        result = agent.save_properties(data)
        #return json.dumps(result,sort_keys=True, indent=4,separators=(',', ': '))
        return json.loads(result)


# SPARQL
class SparqlAPI(Resource):
    def __init__(self):
        super(SparqlAPI, self).__init__()

    def get(self):
        query = request.args['query'] if request.args.get('query') else None
        headers = request.headers
        result = Sparql.query(query, headers)
        response = make_response(result)
        return response


# SPARQL
class ProvenanceAPI(Resource):
    def __init__(self):
        super(ProvenanceAPI, self).__init__()

    def get(self, id):
        prov = Provenance(id=id)
        visited_list = []
        pending_list = []
        graph = prov.outgoing_provenance()
        graph_final = ConjunctiveGraph()
        graph_final.bind('dcterms', 'http://purl.org/dc/terms/')
        graph_final.bind('prov', 'http://www.w3.org/ns/prov#')
        graph_final.bind('bioprov', 'http://bioprov.org/')
        graph_final.bind('teste', 'http://teste.com/')
        graph_final.bind('foaf', 'http://xmlns.com/foaf/0.1/')
        graph_final.bind('doi', 'http://dx.doi.org/')
        graph_final.bind('worldclim', 'http://worldclim.org/')
        graph_final.bind('sensor.com', 'http://sensor.com/')
        graph_final.bind('tools.com', 'http://tools.com/')
        graph_final.bind('poli', 'http://poli.usp.br/')
        graph_final.bind('ib_usp', 'http://ib.usp.br/')
        graph_final.bind('usp', 'http://usp.br/')
        visited_list.append(id)

        while True:
            for subject, predicate, obj in graph:
                if isinstance(subject, URIRef) and not str(subject) in visited_list:
                    visited_list.append(str(subject))
                    if str(subject) in pending_list:
                        pending_list.remove(str(subject))
                if isinstance(obj, URIRef) and not str(predicate) == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type' and not str(obj) in visited_list:
                    pending_list.append(str(obj))

            graph_final += graph
            graph = ConjunctiveGraph()
            if len(pending_list) > 0:
                for item in pending_list:
                    prov.id = item
                    graph += prov.outgoing_provenance()
                pending_list = []
            else:
                break

        prov.id = id
        graph_final += prov.incoming_provenance()

        if 'format' not in request.args or request.args['format'] == 'json':
            return json.loads(graph_final.serialize(format="json-ld", auto_compact=True, indent=4).decode())

        elif request.args['format'] == 'report':
            document = prov.parser_prov_output(graph_final)
            response = make_response(document.get_provn())
            response.headers['content-type'] = 'text/plain'
            return response

        elif request.args['format'] == 'svg':
            document = prov.parser_prov_output(graph_final)
            dot = prov_to_dot(document)
            file_name = app.config['DOWNLOAD_PATH'] + 'graph_{0}.svg'.format(str(id[id.rfind('/')+1:]))
            dot.write(file_name, format='svg')
            return send_file(file_name, mimetype='image/svg+xml')

        elif request.args['format'] == 'png':
            document = prov.parser_prov_output(graph_final)
            dot = prov_to_dot(document)
            file_name = app.config['DOWNLOAD_PATH'] + 'graph_{0}.png'.format(str(id[id.rfind('/')+1:]))
            dot.write(file_name, format='png')
            # filename = Path('bioprov/' + file_name)
            return send_file(file_name, mimetype='image/png')

        elif request.args['format'] == 'rdf':
            response = make_response(graph_final.serialize(format="turtle"))
            response.headers['content-type'] = 'text/plain'
            return response

        else:
            abort(404, message="Format {} invalid.".format(request.args['format']))
