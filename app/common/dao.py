__author__ = 'Agnei Silva'

from franz.openrdf.sail.allegrographserver import AllegroGraphServer
from franz.openrdf.repository.repository import Repository
from franz.miniclient import repository
from franz.openrdf.query.query import QueryLanguage
from franz.openrdf.model import URI
from franz.openrdf.vocabulary.rdf import RDF
from franz.openrdf.vocabulary.rdfs import RDFS
from franz.openrdf.vocabulary.owl import OWL
from franz.openrdf.vocabulary.xmlschema import XMLSchema
from franz.openrdf.query.dataset import Dataset
from franz.openrdf.rio.rdfformat import RDFFormat
from franz.openrdf.rio.rdfwriter import  NTriplesWriter
from franz.openrdf.rio.rdfxmlwriter import RDFXMLWriter
import os, urllib, datetime, time, sys
from config import *


class AllegroGraph (object):

    def __init__(self, server=None, port=None, catalog=None, repository=None, user=None, password=None):
        self.__ag_server = None
        self.__ag_catalog = None
        self.__ag_repository = None
        self.__ag_connection = None
        self.__ag_connection_error = None
        self.__server_name = server if server else AG_HOST
        self.__catalog_name = catalog if catalog else AG_CATALOG
        self.__repository_name = repository if repository else AG_REPOSITORY
        self.__port_number = port if port else AG_PORT
        self.__username = user if user else AG_USER
        self.__password = password if password else AG_PASSWORD

    def get_connection(self):

        try:
            self.__ag_server = AllegroGraphServer(self.__server_name, self.__port_number, self.__username, self.__password)
            self.__ag_catalog = self.__ag_server.openCatalog(self.__catalog_name)
            self.__ag_repository = self.__ag_catalog.getRepository(self.__repository_name, Repository.ACCESS)
            self.__ag_repository.initialize()
            self.__ag_connection = self.__ag_repository.getConnection()
        except Exception, e:
            self.__ag_connection_error = str(e)
            return None, self.__ag_connection_error

        return self.__ag_connection

    def get_errors(self):
        return self.__ag_connection_error

    def close_connection(self):
        self.__ag_connection.close()
        self.__ag_repository.shutDown()
