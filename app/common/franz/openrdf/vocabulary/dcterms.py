# -*- coding: utf-8 -*-
from __future__ import absolute_import
from ..model.value import URI

__author__ = 'Agnei Silva'

NS = "http://purl.org/dc/terms/"

class DCTERMS:
    """
    A 'static' class containing Dublin Core Terms (DCTERMS)
    More Info: http://dublincore.org/documents/2012/06/14/dcmi-terms/?v=terms#
    """

    # Properties in the /terms/ namespace
    Namespace = NS
    abstract = URI(namespace=NS, localname="abstract")
    accessRights = URI(namespace=NS, localname="accessRights")
    accrualMethod = URI(namespace=NS, localname="accrualMethod")
    accrualPeriodicity = URI(namespace=NS, localname="accrualPeriodicity")
    accrualPolicy = URI(namespace=NS, localname="accrualPolicy")
    alternative = URI(namespace=NS, localname="alternative")
    audience = URI(namespace=NS, localname="audience")
    available = URI(namespace=NS, localname="available")
    bibliographicCitation = URI(namespace=NS, localname="bibliographicCitation")
    conformsTo = URI(namespace=NS, localname="conformsTo")
    contributor = URI(namespace=NS, localname="contributor")
    coverage = URI(namespace=NS, localname="coverage")
    created = URI(namespace=NS, localname="created")
    creator = URI(namespace=NS, localname="creator")
    date = URI(namespace=NS, localname="date")
    dateAccepted = URI(namespace=NS, localname="dateAccepted")
    dateCopyrighted = URI(namespace=NS, localname="dateCopyrighted")
    dateSubmitted = URI(namespace=NS, localname="dateSubmitted")
    description = URI(namespace=NS, localname="description")
    educationLevel = URI(namespace=NS, localname="educationLevel")
    extent = URI(namespace=NS, localname="extent")
    format = URI(namespace=NS, localname="format")
    hasFormat = URI(namespace=NS, localname="hasFormat")
    hasPart = URI(namespace=NS, localname="hasPart")
    hasVersion = URI(namespace=NS, localname="hasVersion")
    identifier = URI(namespace=NS, localname="identifier")
    instructionalMethod = URI(namespace=NS, localname="instructionalMethod")
    isFormatOf = URI(namespace=NS, localname="isFormatOf")
    isPartOf = URI(namespace=NS, localname="isPartOf")
    isReferencedBy = URI(namespace=NS, localname="isReferencedBy")
    isReplacedBy = URI(namespace=NS, localname="isReplacedBy")
    isRequiredBy = URI(namespace=NS, localname="isRequiredBy")
    issued = URI(namespace=NS, localname="issued")
    isVersionOf = URI(namespace=NS, localname="isVersionOf")
    language = URI(namespace=NS, localname="language")
    license = URI(namespace=NS, localname="license")
    mediator = URI(namespace=NS, localname="mediator")
    medium = URI(namespace=NS, localname="medium")
    modified = URI(namespace=NS, localname="modified")
    provenance = URI(namespace=NS, localname="provenance")
    publisher = URI(namespace=NS, localname="publisher")
    references = URI(namespace=NS, localname="references")
    relation = URI(namespace=NS, localname="relation")
    replaces = URI(namespace=NS, localname="replaces")
    requires = URI(namespace=NS, localname="requires")
    rights = URI(namespace=NS, localname="rights")
    rightsHolder = URI(namespace=NS, localname="rightsHolder")
    source = URI(namespace=NS, localname="source")
    spatial = URI(namespace=NS, localname="spatial")
    subject = URI(namespace=NS, localname="subject")
    tableOfContents = URI(namespace=NS, localname="tableOfContents")
    temporal = URI(namespace=NS, localname="temporal")
    title = URI(namespace=NS, localname="title")
    type = URI(namespace=NS, localname="type")
    valid = URI(namespace=NS, localname="valid")

    ## map of uri strings to URI objects:
    uristr2obj = {}

for name, uri in DCTERMS.__dict__.iteritems():
    if isinstance(uri,URI):
        DCTERMS.uristr2obj[str(uri)] = uri
