# -*- coding: utf-8 -*-
from __future__ import absolute_import
from ..model.value import URI

__author__ = 'Agnei Silva'

NS = "http://www.w3.org/ns/prov#"

class PROV:
    """
    A 'static' class containing PROV URIs
    """

    # Basic Classes and Properties
    Namespace = NS
    Entity = URI(namespace=NS, localname="Entity")
    Agent = URI(namespace=NS, localname="Agent")
    Activity = URI(namespace=NS, localname="Activity")
    wasGeneratedBy = URI(namespace=NS, localname="wasGeneratedBy")
    wasDerivedFrom = URI(namespace=NS, localname="wasDerivedFrom")
    wasAttributedTo = URI(namespace=NS, localname="wasAttributedTo")
    StartedAtTime = URI(namespace=NS, localname="StartedAtTime")
    used = URI(namespace=NS, localname="used")
    wasInformedBy = URI(namespace=NS, localname="wasInformedBy")
    endedAtTime = URI(namespace=NS, localname="endedAtTime")
    wasAssociatedWith = URI(namespace=NS, localname="wasAssociatedWith")
    actedOnHehalfOf = URI(namespace=NS, localname="actedOnHehalfOf")
    Collection = URI(namespace=NS, localname="Collection")
    EmptyCollection = URI(namespace=NS, localname="EmptyCollection")
    Bundle = URI(namespace=NS, localname="Bundle")
    Person = URI(namespace=NS, localname="Person")
    SoftwareAgent = URI(namespace=NS, localname="SoftwareAgent")
    Organization = URI(namespace=NS, localname="Organization")
    Location = URI(namespace=NS, localname="Location")
    alternateOf = URI(namespace=NS, localname="alternateOf")
    specializationOf = URI(namespace=NS, localname="specializationOf")
    generatedAtTime = URI(namespace=NS, localname="generatedAtTime")
    hadPrimarySource = URI(namespace=NS, localname="hadPrimarySource")
    value = URI(namespace=NS, localname="value")
    wasQuotedFrom = URI(namespace=NS, localname="wasQuotedFrom")
    wasRevisionOf = URI(namespace=NS, localname="wasRevisionOf")
    invalidatedAtTime = URI(namespace=NS, localname="invalidatedAtTime")
    wasInvalidatedBy = URI(namespace=NS, localname="wasInvalidatedBy")
    hadMember = URI(namespace=NS, localname="hadMember")
    wasStartedBy = URI(namespace=NS, localname="wasStartedBy")
    wasEndedBy = URI(namespace=NS, localname="wasEndedBy")
    invalidated = URI(namespace=NS, localname="invalidated")
    influenced = URI(namespace=NS, localname="influenced")
    atLocation = URI(namespace=NS, localname="atLocation")
    generated = URI(namespace=NS, localname="generated")
    Influence = URI(namespace=NS, localname="Influence")
    EntityInfluence = URI(namespace=NS, localname="EntityInfluence")
    Usage = URI(namespace=NS, localname="Usage")
    Start = URI(namespace=NS, localname="Start")
    End = URI(namespace=NS, localname="End")
    Derivation = URI(namespace=NS, localname="Derivation")
    PrimarySource = URI(namespace=NS, localname="PrimarySource")
    Quotation = URI(namespace=NS, localname="Quotation")
    Revision = URI(namespace=NS, localname="Revision")
    ActivityInfluence = URI(namespace=NS, localname="ActivityInfluence")
    Generation = URI(namespace=NS, localname="Generation")
    Communication = URI(namespace=NS, localname="Communication")
    Invalidation = URI(namespace=NS, localname="Invalidation")
    AgentInfluence = URI(namespace=NS, localname="AgentInfluence")
    Attribution = URI(namespace=NS, localname="Attribution")
    Association = URI(namespace=NS, localname="Association")
    Plan = URI(namespace=NS, localname="Plan")
    wasInfluencedBy = URI(namespace=NS, localname="wasInfluencedBy")
    qualifiedInfluence = URI(namespace=NS, localname="qualifiedInfluence")
    qualifiedGeneration = URI(namespace=NS, localname="qualifiedGeneration")
    qualifiedDerivation = URI(namespace=NS, localname="qualifiedDerivation")
    qualifiedPrimarySource = URI(namespace=NS, localname="qualifiedPrimarySource")
    qualifiedQuotation = URI(namespace=NS, localname="qualifiedQuotation")
    qualifiedRevision = URI(namespace=NS, localname="qualifiedRevision")
    qualifiedAttribution = URI(namespace=NS, localname="qualifiedAttribution")
    qualifiedInvalidation = URI(namespace=NS, localname="qualifiedInvalidation")
    qualifiedStart = URI(namespace=NS, localname="qualifiedStart")
    qualifiedUsage = URI(namespace=NS, localname="qualifiedUsage")
    qualifiedCommunication = URI(namespace=NS, localname="qualifiedCommunication")
    qualifiedAssociation = URI(namespace=NS, localname="qualifiedAssociation")
    qualifiedEnd = URI(namespace=NS, localname="qualifiedEnd")
    qualifiedDelegation = URI(namespace=NS, localname="qualifiedDelegation")
    influencer = URI(namespace=NS, localname="influencer")
    entity = URI(namespace=NS, localname="entity")
    hadUsage = URI(namespace=NS, localname="hadUsage")
    hadGeneration = URI(namespace=NS, localname="hadGeneration")
    activity = URI(namespace=NS, localname="activity")
    agent = URI(namespace=NS, localname="agent")
    hadPlan = URI(namespace=NS, localname="hadPlan")
    hadActivity = URI(namespace=NS, localname="hadActivity")
    atTime = URI(namespace=NS, localname="atTime")
    hadRole = URI(namespace=NS, localname="hadRole")




    ## map of uri strings to URI objects:
    uristr2obj = {}

for name, uri in PROV.__dict__.iteritems():
    if isinstance(uri,URI):
        PROV.uristr2obj[str(uri)] = uri
