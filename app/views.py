__author__ = 'Agnei Silva'

# Modules
from app import api, auth
from app.resources.models import *
from flask import render_template, g
from app.resources.resources import UsersAPI, UserAPI, BundleAPI, BundlesAPI, ResourcesAPI, ResourceAPI, EntitiesAPI, \
     EntityAPI, ActivitiesAPI, ActivityAPI, AgentsAPI, AgentAPI, TokenAPI, FilesAPI, BundleDataAPI, SparqlAPI, \
     ProvenanceAPI


# Authenticate Login/Password or Token
@auth.verify_password
def verify_password(login_or_token, password):

    # first try to authenticate by token
    user = User.verify_auth_token(login_or_token)

    if not user:
        # try to authenticate with login/password
        user = User.query.filter_by(login=login_or_token).first()

        if not user or not user.verify_password(password):
            return False

    g.user = user

    return True

# Main Page
@app.route('/')
@app.route('/index')
# @auth.login_required
def index():
    user = {'nickname': 'Agnei Silva'}  # fake user
    return render_template('index.html',
                           title='Home',
                           user=user)


# Main Page
@app.route('/config')
def config():
    user = {
             'SQLALCHEMY_DATABASE_URI': app.config['SQLALCHEMY_DATABASE_URI'],
             'WTF_CSRF_ENABLED': app.config['WTF_CSRF_ENABLED'],
             'SECRET_KEY': app.config['SECRET_KEY'],
             'CURRENT_DIRECTORY': app.config['CURRENT_DIRECTORY'],
             'AG_HOST': app.config['AG_HOST'],
             'AG_PORT': app.config['AG_PORT'],
             'AG_REPOSITORY': app.config['AG_REPOSITORY'],
             'AG_USER': app.config['AG_USER'],
             'AG_PASSWORD': app.config['AG_PASSWORD'],
             'AG_REST_ENDPOINT': app.config['AG_REST_ENDPOINT'],
             'AG_SPARQL_ENDPOINT': app.config['AG_SPARQL_ENDPOINT'],
             'RAISE_EXCEPTION_ON_VERIFY_FAILURE': app.config['RAISE_EXCEPTION_ON_VERIFY_FAILURE'],
             'DOWNLOAD_PATH': app.config['DOWNLOAD_PATH'],
             'UPLOAD_FOLDER': app.config['UPLOAD_FOLDER']
           }  # fake user
    return render_template('index.html',
                           title='Home',
                           user=user)

# add resources to App
api.add_resource(UsersAPI, '/api/users', endpoint='users')
api.add_resource(UserAPI, '/api/users/<int:id>', endpoint='user')

api.add_resource(TokenAPI, '/api/token', endpoint='token')

api.add_resource(ResourcesAPI, '/api/resources', endpoint='resources')
api.add_resource(ResourceAPI, '/api/resources/<path:id>', endpoint='resource')

api.add_resource(BundlesAPI, '/api/bundles/', endpoint='bundles')
api.add_resource(BundleAPI, '/api/bundles/<path:id>', endpoint='bundle')
api.add_resource(BundleDataAPI, '/api/bundles/<path:id>/data', endpoint='bundleData')

api.add_resource(EntitiesAPI, '/api/entities/', endpoint='entities')
api.add_resource(EntityAPI, '/api/entities/<path:id>', endpoint='entity')

api.add_resource(FilesAPI, '/api/files/', endpoint='files')

api.add_resource(ActivitiesAPI, '/api/activities/', endpoint='activities')
api.add_resource(ActivityAPI, '/api/activities/<path:id>', endpoint='activity')

api.add_resource(AgentsAPI, '/api/agents/', endpoint='agents')
api.add_resource(AgentAPI, '/api/agents/<path:id>', endpoint='agent')

api.add_resource(SparqlAPI, '/api/sparql', endpoint='sparql')

api.add_resource(ProvenanceAPI, '/api/provenance/<path:id>', endpoint='provenance')

