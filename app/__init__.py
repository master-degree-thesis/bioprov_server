__author__ = 'Agnei Silva'

# Modules
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from flask_httpauth import HTTPBasicAuth

# Application Initializations
app = Flask(__name__)
api = Api(app)
auth = HTTPBasicAuth()
app.config.from_object('config')
db = SQLAlchemy(app)


from app import views
from app.resources import models
from app.resources.resources import *
